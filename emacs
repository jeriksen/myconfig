;; path where settings files are kept
(add-to-list 'load-path "~/myconfig/emacsfiles")

(require 'el-get-settings) ;; this needs to be first
(require 'general-settings)
(require 'python-settings)
(require 'ispell-settings)
(require 'latex-settings)


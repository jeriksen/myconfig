; set PATH, because we don't load .bashrc
; function from https://gist.github.com/jakemcc/3887459
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (shell-command-to-string "$SHELL -i -c 'echo $PATH'")))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))
 
(if window-system (set-exec-path-from-shell-PATH))

; don't show the startup screen
(setq inhibit-startup-screen 1)

; don't show the menu bar
(menu-bar-mode 0)

; don't show the tool bar
(require 'tool-bar)
(tool-bar-mode 0)

; number of characters until the fill column 
(setq-default fill-column 79)

; each line of text gets one line on the screen (i.e., text will run
; off the left instead of wrapping around onto a new line)
;(setq-default truncate-lines 1)
; truncate lines even in partial-width windows
;(setq truncate-partial-width-windows 1)

; default window width and height
(defun custom-set-frame-size ()
  (add-to-list 'default-frame-alist '(height . 53))
  (add-to-list 'default-frame-alist '(width . 178)))
(custom-set-frame-size)
(add-hook 'before-make-frame-hook 'custom-set-frame-size)

; always use spaces, not tabs, when indenting
(setq-default indent-tabs-mode nil)
 
; show the current line and column numbers in the stats bar as well
(line-number-mode 1)
(column-number-mode 1)

; don't blink the cursor
(blink-cursor-mode 0)

; make sure transient mark mode is enabled (it should be by default,
; but just in case)
(transient-mark-mode 1)

; if there is size information associated with text, change the text
; size to reflect it
(size-indication-mode 1)

; Temporary files cluttering up the space are annoying.  Here's how we
; can deal with them -- create a directory in your home directory, and
; save to there instead!  No more random ~ files.
(defvar user-temporary-file-directory "~/.emacs-autosaves/")
(make-directory user-temporary-file-directory t)
(setq backup-by-copying t)
(setq backup-directory-alist
      `(("." . ,user-temporary-file-directory)
        (tramp-file-name-regexp nil)))
(setq auto-save-list-file-prefix
      (concat user-temporary-file-directory ".auto-saves-"))
(setq auto-save-file-name-transforms
      `((".*" ,user-temporary-file-directory t)))

;; ido mode by defult
(require 'ido)
(ido-mode 1)

;; highlite pair of parenthesis
(require 'paren)
(show-paren-mode 1)

;(require 'no-easy-keys)
;(no-easy-keys 1)

; various key bindings
(global-set-key "\C-q" 'backward-kill-word)
(global-set-key "\C-w" 'delete-backward-char)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key [f7] 'compile)
(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

;; function from misc library
(load-library "misc")
(global-set-key "\C-v" 'scroll-up-half)
(global-set-key "\M-v" 'scroll-down-half)
(global-set-key "\C-cR" 'rename-file-and-buffer)
(global-set-key "\C-cM" 'move-file-and-buffer)


(defalias  'yes-or-no-p 'y-or-n-p)

(provide 'general-settings)

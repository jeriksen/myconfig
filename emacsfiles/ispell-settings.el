(require 'ispell)

(setq-default ispell-program-name "/usr/local/bin/aspell")
(setq ispell-dictionary "english")
(setq-default ispell-list-command "list")

(provide 'ispell-settings)

;; update el-get
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))
(add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")
(setq
 el-get-sourcesn
 '(el-get; el-get is self-hosting
   (:name magit; git meet emacs, and a binding
            :after (progn (global-set-key (kbd "C-x g") 'magit-status)))))
(el-get 'sync)


(provide 'el-get-settings)
